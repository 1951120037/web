<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tất cả sản phẩm</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/custom.css" />
    <link rel="stylesheet" href="css/sanpham.css"/>
</head>
<body>
<!-- main -->
<?php 
error_reporting(E_ALL ^ E_WARNING); 
 if($_SESSION['user_id'])
{
    include("headerlogin.php");
}
 else{
     
    include("header.php");
}
?>
    <div class=" demo-2">
        <div id="slider" class="sl-slider-wrapper">
            <div class="sl-slider">
                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25"
                    data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                    <div class="sl-slide-inner">
                        <div class="bg-img bg-img-5 bg-find"></div>
                        <h2 style="font-size=17px;"> Sản Phẩm Độc Quyền Đây Nhé </h2>
                        <blockquote>
                            <a href="php/productdetails.php"> Mau lựa kẻo hết</a><br>
                            <cite>spidermoon</cite>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <main class="mainwrap">
    <!-- body -->
    <div class="main-header">
        <img class="spimage" src="image/cee3e28827ab4cecd9ca6ebe179120012.jpeg"  alt="" />   
	    <div class="containerbox header-background">
		    <h2>Tất cả sản phẩm</h2>
	    </div>
    </div>
    <div class="containerbox">
        <div class="row">
            <div class="bodyshop col-md-9 col-md-push-3">
                <!-- Header right row -->
                <div class="product-header-actions">
                    <!-- Header row -->
                    <div class="form-inline">
                        <div class="row">
                            <!-- Trang chủ/Sản phẩm -->
                            <div class="col-sm-6 col-xs-6">
                                <ol class="breadcrumb">
								    <li><a href="/">Trang chủ</a></li>
								    <li class="active"><span>Tất cả sản phẩm </span></li>
					            </ol>
                            </div>
                            <!-- Row Button -->
                            <div class="col-md-6 col-sm-6 col-xs-6 f-right">
                                <div class="form-show-sort clear-fix">
                                    <div class="collect-sorting form-group pull-rigth f-right text-right">
                                        <div class="dropdown">
                                            <button class="dropdown-toggle">      
                                                <span class="dropdown-label">Đặc biệt</span>     
                                            </button>
                                            <ul class="dropdown-content dropdown-menu active">
                                                <li class="active"><a href="manual">Đặc biệt</a></li>   
                                                <li><a href="best-selling">Mua nhiều nhất</a></li>
                                                <li><a href="title-ascending">A-Z</a></li>
                                                <li><a href="price-descending">Giá: Cao đến thấp</a></li>
                                                <li><a href="price-ascending">Giá: Thấp đến cao</a></li>
                                                <li><a href="created-ascending">Cũ nhất đến mới nhất</a></li>
                                                <li><a href="created-descending">Mới nhất đến cũ nhất</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Text -->
                                    <p class="text-muted f-right">
								        <span>Hiện</span> 
								        1 đến 12
									    <span>của</span>
									    129 sản phẩm
								    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Body right row -->
                <div class="collection-products">
                    <div class="collection-grid">
                        <div class="products-grid">
                            <div class="products products-grip-wrapper">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-6 product-wrap product-wrap1">
                                        <div class="product product-grid product-item clearfix single-product sss">
                                            <div class="product-inner" >
                                                <div class="product-media">
                                                    <div class="product-img product-thumbnail img-full">
                                                        <a href="#">
                                                            <img class="img-loop lazyloaed current" height="300" width="300" alt=" Áo Thun Gân Degrey - ATGD " data-srcset="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_grande.jpg" src="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_grande.jpg">
                                                        </a>
                                                    </div>
                                                    <div class="product-hover">
                                                        <div class="product-actions"> 
                                                            <a href="javascript:void(0)" class="awe-button product-quick-view btn-quickview" > 
                                                                <i class="line fa fa-eye" aria-hidden="true"></i> 
                                                            </a>
                                                            <form action="/cart/add" method="post" class="variants AddToCartForm-1036755690" enctype="multipart/form-data">
                                                                <a class="btn-select-option product-add-cart" href="/products/ao-thun-gan-degrey-atgd">
                                                                    <i class="line fa fa-cart-plus" aria-hidden="true"></i>
                                                                </a> 
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-loop-variants">
                                                    <div class="product-color-select row"> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_icon.jpg" data-image="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_large.jpg" src="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_icon.jpg" data-image="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_large.jpg" src="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_icon.jpg" data-image="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_large.jpg" src="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select active"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_icon.jpg" data-image="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_large.jpg" src="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_icon.jpg"> </a>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="product-body text-center">
                                                    <h3 class="product-name">
                                                        <a href="/collections/tat-ca-san-pham/products/ao-thun-gan-degrey-atgd" title="Áo Thun Gân Degrey - ATGD">Áo Thun Gân Degrey - ATGD</a>
                                                    </h3>
                                                    <div class="product-price"> 
                                                        <span class="amout"> 
                                                            <span>290,000₫</span>  
                                                        </span>
                                                    </div> 
                                                    <a class="ts-viewdetail hidden" href="/collections/tat-ca-san-pham/products/ao-thun-gan-degrey-atgd">
                                                        <span class="icon icon-arrows-slim-right"></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product__tag">   </div>
                                        </div>    
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6 product-wrap product-wrap1">
                                        <div class="product product-grid product-item clearfix single-product sss">
                                            <div class="product-inner" >
                                                <div class="product-media">
                                                    <div class="product-img product-thumbnail img-full">
                                                        <a href="#">
                                                            <img class="img-loop lazyloaed current" height="300" width="300" alt=" Áo Thun Gân Degrey - ATGD " data-srcset="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_grande.jpg" src="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_grande.jpg">
                                                        </a>
                                                    </div>
                                                    <div class="product-hover">
                                                        <div class="product-actions"> 
                                                            <a href="javascript:void(0)" class="awe-button product-quick-view btn-quickview" > 
                                                                <i class="line fa fa-eye" aria-hidden="true"></i> 
                                                            </a>
                                                            <form action="/cart/add" method="post" class="variants AddToCartForm-1036755690" enctype="multipart/form-data">
                                                                <a class="btn-select-option product-add-cart" href="/products/ao-thun-gan-degrey-atgd">
                                                                    <i class="line fa fa-cart-plus" aria-hidden="true"></i>
                                                                </a> 
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-loop-variants">
                                                    <div class="product-color-select row"> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_icon.jpg" data-image="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_large.jpg" src="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_icon.jpg" data-image="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_large.jpg" src="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_icon.jpg" data-image="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_large.jpg" src="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select active"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_icon.jpg" data-image="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_large.jpg" src="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_icon.jpg"> </a>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="product-body text-center">
                                                    <h3 class="product-name">
                                                        <a href="/collections/tat-ca-san-pham/products/ao-thun-gan-degrey-atgd" title="Áo Thun Gân Degrey - ATGD">Áo Thun Gân Degrey - ATGD</a>
                                                    </h3>
                                                    <div class="product-price"> 
                                                        <span class="amout"> 
                                                            <span>290,000₫</span>  
                                                        </span>
                                                    </div> 
                                                    <a class="ts-viewdetail hidden" href="/collections/tat-ca-san-pham/products/ao-thun-gan-degrey-atgd">
                                                    <span class="icon icon-arrows-slim-right"></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product__tag">   </div>
                                        </div>        
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6 product-wrap product-wrap1">
                                        <div class="product product-grid product-item clearfix single-product sss">
                                            <div class="product-inner" >
                                                <div class="product-media">
                                                    <div class="product-img product-thumbnail img-full">
                                                        <a href="#">
	                                                        <img class="img-loop img-hover lazyloaed" height="300" width="300" alt=" Áo Thun Gân Degrey - ATGD " data-srcset="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_grande.jpg" src="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_grande.jpg">
                                                        </a>
                                                    </div>
                                                    <div class="product-hover">
                                                        <div class="product-actions"> 
                                                            <a href="javascript:void(0)" class="awe-button product-quick-view btn-quickview" > 
                                                                <i class="line fa fa-eye" aria-hidden="true"></i> 
                                                            </a>
                                                            <form action="/cart/add" method="post" class="variants AddToCartForm-1036755690" enctype="multipart/form-data">
                                                                <a class="btn-select-option product-add-cart" href="/products/ao-thun-gan-degrey-atgd">
                                                                    <i class="line fa fa-cart-plus" aria-hidden="true"></i>
                                                                </a> 
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-loop-variants">
                                                    <div class="product-color-select row"> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_icon.jpg" data-image="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_large.jpg" src="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_icon.jpg" data-image="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_large.jpg" src="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_icon.jpg" data-image="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_large.jpg" src="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select active"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_icon.jpg" data-image="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_large.jpg" src="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_icon.jpg"> </a>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="product-body text-center">
                                                    <h3 class="product-name">
                                                        <a href="/collections/tat-ca-san-pham/products/ao-thun-gan-degrey-atgd" title="Áo Thun Gân Degrey - ATGD">Áo Thun Gân Degrey - ATGD</a>
                                                    </h3>
                                                    <div class="product-price"> 
                                                        <span class="amout"> 
                                                            <span>290,000₫</span>  
                                                        </span>
                                                    </div> 
                                                    <a class="ts-viewdetail hidden" href="/collections/tat-ca-san-pham/products/ao-thun-gan-degrey-atgd">
                                                        <span class="icon icon-arrows-slim-right"></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product__tag">   </div>
                                        </div>        
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6 product-wrap product-wrap1">
                                        <div class="product product-grid product-item clearfix single-product sss">
                                            <div class="product-inner" >
                                                <div class="product-media">
                                                    <div class="product-img product-thumbnail img-full">
                                                        <a href="#">
	                                                        <img class="img-loop img-hover lazyloaed" height="300" width="300" alt=" Áo Thun Gân Degrey - ATGD " data-srcset="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_grande.jpg" src="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_grande.jpg">
                                                        </a>
                                                    </div>
                                                    <div class="product-hover">
                                                        <div class="product-actions"> 
                                                            <a href="javascript:void(0)" class="awe-button product-quick-view btn-quickview" > 
                                                                <i class="line fa fa-eye" aria-hidden="true"></i> 
                                                            </a>
                                                            <form action="/cart/add" method="post" class="variants AddToCartForm-1036755690" enctype="multipart/form-data">
                                                                <a class="btn-select-option product-add-cart" href="/products/ao-thun-gan-degrey-atgd">
                                                                    <i class="line fa fa-cart-plus" aria-hidden="true"></i>
                                                                </a> 
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-loop-variants">
                                                    <div class="product-color-select row"> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_icon.jpg" data-image="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_large.jpg" src="//product.hstatic.net/1000281824/product/346_98f99dc2483f40a6898be140422d030d_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_icon.jpg" data-image="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_large.jpg" src="//product.hstatic.net/1000281824/product/347_d9c2da1b00e44e7b8fa0c2fc8b04db06_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_icon.jpg" data-image="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_large.jpg" src="//product.hstatic.net/1000281824/product/350_84b738aed6364a46bf08a49a4de68f14_icon.jpg"> </a>
                                                        </div> 
                                                        <div class="col-xs-2 product-color-select active"> 
                                                            <a class="product-color-select-item detail"> 
                                                                <img class="img-fluid lazyloaed" height="22" width="22" data-srcset="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_icon.jpg" data-image="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_large.jpg" src="//product.hstatic.net/1000281824/product/349_fa573918b08e417896d5f4e107e1243e_icon.jpg"> </a>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="product-body text-center">
                                                    <h3 class="product-name">
                                                        <a href="/collections/tat-ca-san-pham/products/ao-thun-gan-degrey-atgd" title="Áo Thun Gân Degrey - ATGD">Áo Thun Gân Degrey - ATGD</a>
                                                    </h3>
                                                    <div class="product-price"> 
                                                        <span class="amout"> 
                                                            <span>290,000₫</span>  
                                                        </span>
                                                    </div> 
                                                    <a class="ts-viewdetail hidden" href="/collections/tat-ca-san-pham/products/ao-thun-gan-degrey-atgd">
                                                        <span class="icon icon-arrows-slim-right"></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product__tag">   </div>
                                        </div>    
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>                
            </div>
            <!-- Slide bar -->
            <div class="slidebar-row col-md-3 col-md-pull-9  collect-slidebar">
                <div id="widgets-filters" class="widgets-filter">
                    <!-- searching -->
                    <div class="filter-block">
                        <div class="widget woocommerce search-widget">
                            <div class="widgets-content">
                                <form class="search-form" id="searchform" action="/search" method="get" role="search">
                                    <input type="text" placeholder="Tìm kiếm" name="q">
                                    <span><button type="submit" id="submit_btn" class="search-submit"><i class="fas fa-search"></i>
                                        </button>
                                    </span>
                                    <input type="hidden" value="product" name="post_type">
                                </form>
                            </div>
                        </div>
                        <!-- Danh mục -->
                        <div class="widget woocommerce widget-product">
                            <h5 class="widget-title">Danh mục</h5>
                            <ul class="toggle-menu">
                                <li class="active">
                                    <a href="">TẤT CẢ SẢN PHẨM | ALL</a>
                                </li>
                                <li>
                                    <a href="#">ÁO | CLOTHES</a>
                                </li>
                                <li>
                                    <a href="#">Quần | PANTS</a>
                                </li>
                                <li>
                                    <a href="#">Ba lô | Backpacks</a>
                                </li>
                            </ul>
                        </div>
                               
                        <!-- Giá tiền -->
                        <div class="widget woocommerce widget_product_prices filter-custom filter-tag">
                            <h5 class="widget-title">
                                Giá tiền
                            <a href="javascript:void(0)" class="clear" style="display:none">Clear</a>
                            </h5>
                            <div class="widget-content">    
                            <ul></ul>
                        </div>
                        <div class="widget woocommerce widget_product_prices filter-custom filter-tag">
                            <h5 class="widget-title">Màu sắc
                                <a href="javascript:void(0)" class="clear" style="display:none">
                                    <i class="fa fa-times-circle"></i>
                                </a>                              
                            </h5>
                            <div class="widget-content">    
                                <ul></ul>
                            </div>
                        </div>
                    </div>
                </div>
                													
            </div>
        </div>    
    </div>        
</main> 

<?php include_once "footer.php"; ?>
<script src="js/js_search.js"></script>
</body>

</html>