<ul class="pagination">
        <li><a href="/foo/page/7">&laquo; Previous</a></li>
        <li><a href="?per_page=4&page=1">1</a></li>
        <li class="disabled"><span>...</span></li>
        <li><a href="/foo/page/5">5</a></li>
        <li><a href="/foo/page/6">6</a></li>
        <li><a href="/foo/page/7">7</a></li>
        <li class="active"><a href="/foo/page/8">8</a></li>
        <li><a href="/foo/page/9">9</a></li>
        <li><a href="/foo/page/10">10</a></li>
        <li><a href="/foo/page/11">11</a></li>
        <li><a href="/foo/page/12">12</a></li>
        <li class="disabled"><span>...</span></li>
        <li><a href="/foo/page/20">20</a></li>
        <li><a href="/foo/page/9">Next &raquo;</a></li>
</ul>