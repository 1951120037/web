<?php
    session_start();
    include_once "php/connectdb.php";
    include("php/functions.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin khách hàng</title>
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css"
        integrity="sha512-rqQltXRuHxtPWhktpAZxLHUVJ3Eombn3hvk9PHjV/N5DMUYnzKPC1i3ub0mEXgFzsaZNeJcoE0YHq0j/GFsdGg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/custom.css" />
    <link rel="stylesheet" href="css/detailcustomer.css" />
    <script type="text/javascript" src="js/modernizr.custom.79639.js"></script>
</head>
<body>
<?php 
error_reporting(E_ALL ^ E_WARNING); 
 if($_SESSION['user_id'])
 {
    include("headerlogin.php");
}
 else{
     
    include("header.php");
}
 ?>
    <div class=" demo-2">
        <div id="slider" class="sl-slider-wrapper">
            <div class="sl-slider">
                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25"
                    data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                    <div class="sl-slide-inner">
                        <div class="bg-img bg-img-4 bg-find"></div>
                        <h2>Chi tiết tài khoản khách hàng </h2>
                        <blockquote>
                            <cite>spidermoon</cite>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <label style="padding:30px; font-size:18px; color:#111;"> Thông Tin Khách Hàng
        <?php
        $sql = mysqli_query($conn, "SELECT * FROM user WHERE user_id = {$_SESSION['user_id']}") or die('Có lỗi xin kiểm tra lại!');
        $Nameshow = mysqli_fetch_array($sql); 
        echo $Nameshow['username'];
        ?>
    </label>

    <?php
    echo"
    <div class='wrapper-cus'>
        <div class='title-cus'>
            Thông tin
            <div class='error'></div>
        </div>
        <form action= 'php/fixdata-user.php' method='post' class='form-cus'>
            <div class='inputfield-cus'>
                <label>FULL NAME</label>
                <input type='text' class='input-cus' value= '". $Nameshow['fullname']."' name='fullname'>
            </div>
            <div class='inputfield-cus'>
                <label>USER NAME</label>
                <input type='text' class='input-cus' value= '". $Nameshow['username']."' name='username'>
            </div>
            <div class='inputfield-cus'>
                <label>EMAIL</label>
                <input type='text' class='input-cus' value= '". $Nameshow['email']."' name='email'>
            </div>
            <div class='inputfield-cus'>
                <label>PASSWORD</label>
                <input type='password' class='input-cus' value= '". $Nameshow['password']."' name='password'>
            </div>
            <div class='inputfield-cus'>
                <label>PHONE NUMBER</label>
                <input type='text' class='input-cus' value= '". $Nameshow['numberphone']."' name='numberphone'>
            </div>
            <div class='inputfield-cus'>
                <label>ADDRESS</label>
                <input class='textarea-cus' value='" .$Nameshow['address']."' name='address'>
            </div>
            <div class='inputfield-cus'>
                <label>DATE CREATE</label>
                <input type='text' class='input-cus' value= '". $Nameshow['date_created']."' name='date_created'>
            </div>
            <div class='inputfield-cus'>
                <input type='submit' value='Cập nhật Lại' class='btn-cus'>
            </div>
        </form>
    </div>
    ";
    ?>
    <!-- footer -->
    <?php include_once "footer.php";?>
    <!-- <script src="js/detail-cus-fix.js"></script> -->
    <script src="js/js_search.js"></script>
    <script src="js/loginindex.js"></script>
</body>
</html>