
<footer class= "footer">
    <div class="social">
        <a href=""><i class="fab fa-instagram"></i></a>
        <a href=""><i class="fab fa-snapchat"></i></a>
        <a href=""><i class="fab fa-facebook"></i></a>
        <a href=""><i class="fab fa-twitter"></i></a>
    </div>

    <ul class="list">
        <li>
            <a href="">Shop</a>
        </li>
        <li>
            <a href="">Đặt hàng</a>
        </li>
        <li>
            <a href="">Chính sách bảo mật</a>
        </li>
        <li>
            <a href="">Giới thiệu</a>
        </li>
    </ul>

    <p class="copyright">
        Spidermoon @ 2021
    </p>
</footer>
