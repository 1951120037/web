<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng Ký</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" integrity="sha512-rqQltXRuHxtPWhktpAZxLHUVJ3Eombn3hvk9PHjV/N5DMUYnzKPC1i3ub0mEXgFzsaZNeJcoE0YHq0j/GFsdGg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Bootstrap -->
    
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/signup.css" />
</head>
<body>
<?php include_once "header.php"; ?>
<form  class="mainmenu" >
    <div class="container">
        <img class="login-signup-image" src="image/cee3e28827ab4cecd9ca6ebe179120012.jpeg" height="50%" width="50%" alt="" />
        <h2 class="center">Đăng Nhập</h2>
        <div class="sign-login">
            <div class="error-txt"></div>
            <div class="wrap2">
                <label>email</label>
                <input class="email" type="text"  name = "email">
                
                <span class="focus-input2"></span>
            </div>
            <div class="wrap2">
                <label>Password</label>
                <input  class="password" type="password"  name = "password">
                <span class="focus-input2"></span>
            </div>
            <p>Bạn chưa có tài khoản? - <a href="signup.php">Đăng Ký?</a></p>
            <button  class="btn" >Đăng Nhập</button>
        </div>
    </div>  
</form>

<?php include_once "footer.php"; ?>
<script src="js/js_search.js"></script>
<script src="js/login.js"></script>
</body>
</html>