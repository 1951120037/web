const form = document.querySelector(".mainmenu"),
    continuebtn = form.querySelector(".btn"),


    errortext = form.querySelector(".error-txt");
form.onsubmit = (e) => {
    e.preventDefault();
}
continuebtn.onclick = () => {
    // let ajax
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "php/login.php", true);
    xhr.onload = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                let data = xhr.response;
                if (data == "success") {

                    location.href = "http://localhost/web/index.php";

                } else {
                    errortext.textContent = data;
                    errortext.style.display = "block";

                }
            }
        }
    }
    // send data thorght ajax to php
    let formData = new FormData(form); //create form data object

    xhr.send(formData);
}
