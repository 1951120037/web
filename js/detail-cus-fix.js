const formdata = document.getElementsByClassName('.form-cus'),
fixbtn =formdata.querySelector('.btn-cus'),
errortext = document.querySelector('.error');
formdata.onsubmit = (e) => {
    e.preventDefault();
} 
fixbtn.onclick = () => {
    // let ajax
    let xhr1 = new XMLHttpRequest();
    xhr1.open("POST", "php/fixdata-user.php", true);
    console.log(xhr1);
    xhr1.onload = () => {
        if (xhr1.readyState === XMLHttpRequest.DONE) {
            if (xhr1.status === 200) {
                let data = xhr1.response;
                if (data == "success") {
                    location.href = "http://localhost/web/index.php";
                } else {
                    errortext.textContent = data;
                    errortext.style.display = "block";
                }
            }
        }
    }
    // send data thorght ajax to php
    let formData2 = new FormData(formdata); //create form data object

    xhr1.send(formData2);
}