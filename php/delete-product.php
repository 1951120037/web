<?php
// tạo session
// xóa sản phẩm trong giỏ hàng
session_start();
if (isset($_GET['id']) && $_GET['id'] != NULL) {
    // gán id = $_GET['id']
    $id = $_GET['id'];
    if (empty($id)) {
        unset($_SESSION['cart']);
    } else {
        unset($_SESSION['cart'][$id]);
    }
    header('location: ' . $_SERVER['HTTP_REFERER']); //back-page
} else {
    echo "Có lỗi xin kiểm tra lại!";
}
