<?php
//tao phuong thuc them san pham vao gio hang
session_start(); //khoi tao session để lưu trữ
//Hàm isset() được dùng để kiểm tra một biến nào đó đã được khởi tạo hay chua
if (isset($_GET['id']) && $_GET['id'] != NULL) {
    $id = $_GET['id'];
    $total_product = NULL;
    if (!empty($_SESSION['cart'][$id])) {
        $total_product = $_SESSION['cart'][$id] + 1;
    } else {
        $total_product = 1;
        echo "Bạn hãy lựa chọn một sản phẩm";
    }
    $_SESSION['cart'][$id] = $total_product; //lay so luong sp trong gio
    //trở lại
    header('location: ' . $_SERVER['HTTP_REFERER']); 
}
else{echo "Có lỗi xin kiểm tra lại";}
?>