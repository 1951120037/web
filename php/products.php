<!DOCTYPE html>
<html>

<body>
    <section class="content">
        <!-- left - loc san pham theo id -->
        <aside class="filter" id="filter_cate">
            <div class="filter_sp">
                <div class="general">
                    <ul class="menu-left">
                        <?php
                        $menu_left_res = mysqli_query($conn, "SELECT * FROM submenu");
                        echo "<li><a href='#' class='title'>CLOHTES</a>
								<ul class='menu-in-left'>";
                        while ($menu_left_items = mysqli_fetch_array($menu_left_res)) {
                            echo "<li><a href='php/products.php?id_menu=" . $menu_left_items['submenu_id'] . "'>" . $menu_left_items['sub_name'] . " </a></li>";
                        }
                        echo "
								</ul>
						</li>";
                        ?>
                    </ul>
                </div>
            </div>
        </aside>

        <!-- phan trang cho san pham -->
        <aside class="product-page">
            <div class="product-border">
                <?php
                $id_menu = $_GET["id_menu"];
                settype($id_menu, "int");
                // tim tong so records
                $result = mysqli_query($conn, "SELECT count(product_id) AS total FROM sanpham WHERE submenu_id ={$id_menu} or catalog_id = {$id_menu}");
                $row = mysqli_fetch_assoc($result);
                $total_records = $row['total']; //gán tổng số record là total, đếm bằng id sản phẩm
                // tim limit va current page
                $current_page = isset($_GET['page']) ? $_GET['page'] : 1; //kiểm tra $_GET['page'], bấm trang kế có tồn tại không
                $litmit = 8; // tổng số record trên 1 trang
                $total_page = ceil($total_records / $litmit); // tổng số page khi mỗi page hiển thị bao nhiêu sp
                // Giới hạn current_page trong khoảng 1 đến total_page
                if ($current_page > $total_page) {
                    $current_page = $total_page;
                } else if ($current_page < 1) {
                    $current_page = 1;
                }
                // Tìm Start
                $start = ($current_page - 1) * $litmit;
                // Có limit và start rồi thì truy vấn CSDL lấy danh sách sản phẩm
                $result = mysqli_query($conn, "SELECT * FROM sanpham WHERE submenu_id ={$id_menu} or catalog_id = {$id_menu} LIMIT {$start}, {$litmit}");
                ?>

                <?php
                if (!empty($total_records)) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo "<div class='product-item'>";
                        echo "
							<a href='php/productdetails.php?id=" . $row['product_id'] . "' class='images'>
							    <img alt='" . $row['product_name'] . "' src='images/" . $row['product_image'] . "'>
							</a>

							<h2 style='margin-top:0;margin-bottom:0;'>
							    <a title='" . $row['product_name'] . "' href='php/productdetails.php?id=" . $row['product_id'] . "'>" . $row['product_name'] . "</a>
							</h2>

							<div class='price'>" . number_format($row['price']) . " VNĐ</div>
							
                            <div class='ratings'>
								<div class='rating-box'></div>
							</div>
							<a href='php/addtocart.php?id=" . $row['product_id'] . "'><div class='add-cart''><i class='fas fa-shopping-cart'></i>Add to cart </div></a>
							";
                        echo "</div>";
                    }
                } else {
                    echo "Sản phẩm chưa có!";
                }
                ?>

            </div>
            <!--end product-border-->

            <!-- PHÂN TRANG -->
            <div class="paging" style="margin-left: calc(936px/2);">
                <?php
                // PHẦN HIỂN THỊ PHÂN TRANG
                if ($current_page > 1 && $total_page > 1) {
                    // nếu current_page > 1 và total_page > 1 mới hiển thị nút prev
                    echo "<a href='php/products.php?id_menu=" . $id_menu . "&page=" . ($current_page - 1) . "'>
								<b class='prev'></b>
							</a>";
                }
                echo "<ul class='ul-paging'>";
                // Lặp khoảng giữa
                for ($i = 1; $i <= $total_page; $i++) {
                    // Nếu là trang hiện tại thì hiển thị thẻ span
                    // ngược lại hiển thị thẻ a
                    if ($i == $current_page) {
                        echo "<li><span class='color_current'>" . $i . "</span></li>";
                    } else
                        echo '<li><a href="php/products.php?id_menu=' . $id_menu . '&page=' . $i . '">' . $i . '</a></li>';
                }
                echo "</ul>";
                // nếu current_page < $total_page và total_page > 1 mới hiển thị nút prev
                if ($current_page < $total_page  && $total_page > 1) {
                    echo "<a href='php/products.php?id_menu=" . $id_menu . "&page=" . ($current_page + 1) . "'>
							<b class='next'></b>
						</a>";
                }

                ?>
            </div>
            <!--end paging-->
        </aside>


    </section>
</body>

</html>