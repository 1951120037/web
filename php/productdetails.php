<!DOCTYPE html>
<html lang="en">
<?php
// tong so san pham da them vao gio hang
$total_product = 0;
if (!empty($_SESSION['cart'])) $total_product = count($_SESSION['cart']);
?>

<head>
    <!-- <link rel="stylesheet" href="/css/style.css"> -->
</head>

<body>
    <?php
    include_once "header.php";
    include("php/connectdb.php"); //ket noi csdl   
    ?>
    <!-- phan hien thi cho thong tin chi tiet san pham -->
    <div class="navigations">
        <div class="back-room">
            <span class="homepage">
                <a href="https://localhost/web/index.php">Trang chủ</a>
                >

            </span>

            <span class="homepage">
                <?php
                $id = $_GET['id'];
                $product_query = "SELECT * FROM sanpham where product_id =" . $id;
                $product_res = mysqli_query($conn, $product_query) or die("Có lỗi xin kiểm tra lại!");
                while ($product_items = mysqli_fetch_array($product_res)) {
                    echo '' . $product_items["parent_menu_name"] . '';
                    echo '>';
                ?>
            </span>

            <span class="homepage">
                <?php
                    echo '' . $product_items["product_name"] . '';
                    echo ' ›';
                ?>
            </span>
            <span class="tittleRm">
            <?php
                    echo '' . $product_items["product_name"] . '';
                }
            ?>
            </span>

        </div>

    </div>
    <!--end navigation-->

    <section class="product_d content_ld">
        <div class="detail_p">
            <aside class="images_d">
                <div class="images_d_list owl-carousel owl-theme" style="opacity: 1; display: block;">
                    <div class="owl-wrapper-outer">
                        <div class="owl-wrapper" style="width: 712px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);">
                            <div class="owl-item" style="width: 356px;">
                                <?php
                                $id = $_GET['id'];
                                $product_query = "SELECT * FROM sanpham where product_id=$id";
                                $product_res = mysqli_query($conn, $product_query) or die("Có lỗi xin kiểm tra lại!");
                                while ($product_items = mysqli_fetch_array($product_res)) {
                                    echo '<img src="images/' . $product_items["product_image"] . '">';

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <!--end images_d-->
            <aside class="desProduct">
                <aside class="desProduct">
                    <h1 class="detailPT">
                        <?php
                                    echo $product_items["product_name"];

                        ?>
                    </h1>
                    <div class="des">
                        <?php
                                    echo $product_items['description'];

                        ?>
                    </div>
                    <!--des-->
                    <div class="proFtiter">
                        <?php
                                    echo 'Xuất xứ:';
                                    echo "<span>" . $product_items['source'] . "</span>";
                                    echo '&nbsp;&nbsp;Kích thước:';
                                    echo "<span>" . $product_items['size'] . "</span>";
                                    echo '&nbsp;&nbsp;Màu sắc:';
                                    echo "<span>" . $product_items['color'] . "</span>";

                        ?>
                    </div>
                    <!--proFtiter-->

                    <div class="pr_det_price ">
                        <span class="tittle">Giá bán:</span>
                        <!--tittle-->
                        <div class="price">
                        <?php
                                    echo "" . number_format($product_items['price']) . " VNĐ";
                                }
                        ?>
                        </div>
                        <!--end price-->
                    </div>
                    <!--pr_det_price -->
                    <div class="pro_dg">
                        <div class="pro_dg_tt">
                            <div class="pro_dg_tt">
                                <div class='ratings'>
                                    <div class='rating-box'>
                                    </div>
                                    <!--end ratingbox-->
                                </div>
                                <!--end ratings-->
                            </div>
                            <!--end pro_dg_tt-->
                        </div>
                        <!--end pro_dg_tt-->
                        <a target="_blank" href="http://www.facebook.com/sharer.php?u=http://localhost:8080/web/productdetails.php?id=<?php echo $id; ?>" class="fb shareFa"></a>
                        <a target="_blank" href="http://twitter.com/share?url=http://localhost:8080/webbanhang/productdetails.php?id=<?php echo $id; ?>" class="tw shareFa"></a>
                        <a target="_blank" href="https://plus.google.com/share?url=http://sanphammtlpro.16mb.com/san-pham/page-chitiet.php?id=<?php echo $id; ?>" class="gg shareFa"></a>
                    </div>
                    <!--end pro_dg-->

                    <div class="proDha">
                        <a href="php/addtocart.php?id=<?php echo $id ?>">
                            <div class="btDah">
                                <span class="bttOp" style="font-size: 20px; padding-top:10px;">THÊM VÀO GIỎ</span>
                            </div>
                            <!--end btDah-->
                        </a>
                        <div class="yctv"><a href="https://www.facebook.com/CoktailDail/" target="_blank"> YÊU CẦU TƯ VẤN</a> </div>
                        <!--end yctv-->
                    </div>
                    <!--end proDha-->
                    <div class="goituvan">
                        <i class="icon"></i>GỌI TƯ VẤN
                        <span>0855457078</span>
                    </div>
                    <!--goiTongDa-->
                    <div class="hotro">
                        <span>Để các chuyên viên hỗ trợ bạn sản phẩm phù hợp nhất cho nhu cầu và túi tiền của bạn.</span>
                    </div>
                    <!--end hotro-->
                </aside>
                <!--desProduct-->
            </aside>
        </div>

    </section>
</body>

</html>