<?php
// so san pham da add vao cart

$total_product = 0;
if (!empty($_SESSION['cart'])) $total_product = count($_SESSION['cart']);
?>

<head>
    <link rel="stylesheet" type="text/css" href="<?php echo $base; ?>/css/style.css" />
</head>
<header class="header">
    <nav>
        <input id="nav-toggle" type="checkbox">

        <a href="#" class="logo"><i class="fas fa-piggy-bank"></i>FSOCI</a>
        <ul class="links">
            <li><a href="index.php">Trang Chủ</a></li>
            <li><a href="#about">Sự Kiện</a></li>
            <li><a href="sanpham.php">Sản Phẩm</a></li>
            <li><a href="#projects">Hệ Thống</a></li>
            <li><a href="#contact">Giới Thiệu</a></li>
        </ul>
        <label for="nav-toggle" class="icon-burger">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
        </label>
        <div class="icons">
            <div class="fas fa-shopping-cart" id="cart-btn"></div>
            <div class="fas fa-user" id="login-btn"></div>
            <div class="fas fa-search" id="search-btn"></div>
        </div>

        <form action="search.php" method ="post" class="search-form">
            <input type="text" id="search-box" placeholder="search here..." name= "searchBar">
            <input type="submit" id ="searchBtn" value="Find" style="font-size:22px; color:#bda47d">
        </form>
        <form action="" class="login-form">
            <h3>Đăng Nhập</h3>
            <input type="email" placeholder="your email" class="box" name="email">
            <input type="password" placeholder="your password" class="box" name="password">
            <p>forget your password <a href="#">click here</a></p>
            <p>don't have an account <a href="signup.php">create now</a></p>
            <button  type ="submit" id="logbtn" class="btnLogin">Đăng Nhập</button>
        </form>
        <form action="" class="shopping-cart">
            <!-- <div class="box">
                <i class="fas fa-trash"></i>
                <img src="images/ba-lo-1.jpg" alt="">
                <div class="contents">
                    <h3>watermelon</h3>
                    <span class="price">$4.99/-</span>
                    <span class="quantity">qty : 1</span>
                </div>
            </div>
            <div class="box">
                <i class="fas fa-trash"></i>
                <img src="images/ba-lo-2.jpg" alt="">
                <div class="contents">
                    <h3>onion</h3>
                    <span class="price">$4.99/-</span>
                    <span class="quantity">qty : 1</span>
                </div>
            </div>
            <div class="total"> total : $19.69/- </div>
            <a href="#" class="btnCart">checkout</a> -->
            <div id="nr-cart-header" class="nr-cart-header fullheight">
                <div class="widget shoping-cart-widget">
                    <h5>Giỏ hàng</h5>
                    <div class="widget_shopping_cart_content">
                        <!-- <span class="text-muted margin-top-20 block">Giỏ hàng trống</span> -->
                        <ul class="cart_list product_list_widget item" id="cart-item"></ul>
                        <!-- Phần xử lý giỏ hàng -->

                        <div class="cart_div">

                            <a href="<?php echo $base ?>/cart.php" class="cart_top">
                                <span class="count"><?php echo $total_product; ?></span>
                                <span class="tit"></span>

                            </a>
                            <div class="quick_cart">
                                <?php //cap nhat lai gia khi nhap vao so luong
                                if (isset($_POST['update_cart'])) {
                                    foreach ($_SESSION['cart'] as $id => $total_product) //prd la gia tri nhap vao.moi id tuong ung voi so luong nhap vao
                                    {
                                        if (($total_product == 0) and (is_numeric($total_product))) //nhap vao =0 thi xoa san pham do di
                                        {
                                            unset($_SESSION['cart'][$id]);
                                        } elseif (($total_product > 0) and (is_numeric($total_product))) // so luong >0  thi tiep tuc tinh
                                        {
                                            $_SESSION['cart'][$id] = $total_product;
                                        }
                                    }
                                }
                                ?>
                                <form method="post">
                                    <div class="cart_order">
                                        <ul class="top_cart">
                                            <li class="sp">SẢN PHẨM</li>
                                            <li class="dg">ĐƠN GIÁ</li>
                                            <li class="sl">SL</li>
                                            <li class="tt">THÀNH TIỀN</li>
                                        </ul>
                                        <?php
                                        if (isset($_SESSION['cart'])) {
                                            if ($_SESSION['cart'] != NULL) {
                                                foreach ($_SESSION['cart'] as $id => $total_product) {
                                                    $arr_id[] = $id;
                                                }
                                                $string_id = implode(',', $arr_id);
                                                $item_query = "SELECT * FROM sanpham WHERE product_id IN ($string_id) ORDER BY product_id ASC";
                                                $item_result = mysqli_query($conn, $item_query) or die('Có lỗi xin kiểm tra lại!');
                                                while ($rows = mysqli_fetch_array($item_result)) {
                                        ?>
                                                    <ul class="bottom_cart">
                                                        <li class="sp">
                                                            <img src="images/<?php echo $rows['product_image']; ?>" class="cart_image">
                                                            <b class="cart-title"><?php echo $rows['product_name']; ?></b>

                                                            <div class="delete-cart" style="text-align: center;font-size: 16px;"><a href="<?php echo $base ?>/php/delete-product.php?id=<?php echo $rows['product_id']; ?>" class="delete-product">Xóa</a></div>

                                                        </li>
                                                        <li class="dg"><?php echo number_format($rows['price']); ?> VNĐ</li>
                                                        <li class="sl"><input type="text" name="num[<?php echo $rows['product_id']; ?>]" value="<?php echo $_SESSION['cart'][$rows['product_id']]; ?>" size="3" class="update_cart_text" /></li>
                                                        <li class="tt"><?php echo number_format($rows['price'] * $_SESSION['cart'][$rows['product_id']]); ?> VNĐ
                                                        </li>
                                                    </ul>

                                        <?php
                                                }
                                            } else {
                                                echo "<p><div class='no-product'><p style='text-align: center;font-size: 18px; color:#949494'>Giỏ hàng trống</p></div>";
                                            }
                                        } else {
                                            $_SESSION['cart'] = array();
                                        }
                                        ?>
                                        <div class="go_shopping">
                                            <!-- <input type="submit" name="update-the-cart" value="CẬP NHẬT" class="up-cart" />
                                            <a href="<?php echo $base ?>/cart.php" class="goa_shopping">GIỎ HÀNG</a> -->
                                            <p class="buttons">
                                                <a href="<?php echo $base ?>/cart.php" class="button wc-forward">Xem giỏ hàng</a>
                                                <a class="button checkout wc-forward" href="<?php echo $base ?>/php/pay.php">Thanh toán</a>
                                            </p>
                                        </div>
                                        <!-- <p class="total"><strong>Tất cả:</strong> <span class="amount">0₫</span></p> -->

                                    </div>
                                    <!--end cart_order-->
                                </form>
                            </div>
                            <!--End Quick-->
                        </div>
                        <!--end cart_div-->



                    </div>
                </div>
            </div>
            </div>
        </form>
    </nav>
</header>