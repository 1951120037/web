<?php
session_start();
include("php/connectdb.php");
include("php/functions.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TRANG CHỦ</title>
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" integrity="sha512-rqQltXRuHxtPWhktpAZxLHUVJ3Eombn3hvk9PHjV/N5DMUYnzKPC1i3ub0mEXgFzsaZNeJcoE0YHq0j/GFsdGg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Bootstrap -->
   
    <!-- Icon -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/custom.css" />

    <script type="text/javascript" src="js/modernizr.custom.79639.js"></script>
</head>
</head>

<body>
    <?php
    error_reporting(E_ALL ^ E_WARNING); //tránh cảnh cáo
    if ($_SESSION['user_id']) {
        include("headerlogin.php");
    } else {

        include("header.php");
    }
    ?>
    <div class=" demo-2">
        <div id="slider" class="sl-slider-wrapper">

            <div class="sl-slider">

                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                    <div class="sl-slide-inner">
                        <div class="bg-img bg-img-1"></div>
                        <h2>Tất cả sản phẩm</h2>
                        <blockquote>
                            <p>Bấm vào là hiện hết trơn á</p><cite>spidermoon</cite>
                        </blockquote>
                    </div>
                </div>

                <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
                    <div class="sl-slide-inner">
                        <div class="bg-img bg-img-2"></div>
                        <h2>Tất cả sản phẩm</h2>
                        <blockquote>
                            <p>Bấm vào là hiện hết trơn á</p><cite>spidermoon</cite>
                        </blockquote>
                    </div>
                </div>

                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
                    <div class="sl-slide-inner">
                        <div class="bg-img bg-img-3"></div>
                        <h2>Tất cả sản phẩm</h2>
                        <blockquote>
                            <p>Bấm vào là hiện hết trơn á</p><cite>spidermoon</cite>
                        </blockquote>
                    </div>
                </div>

                <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25" data-slice1-scale="2" data-slice2-scale="1">
                    <div class="sl-slide-inner">
                        <div class="bg-img bg-img-4"></div>
                        <h2>Tất cả sản phẩm</h2>
                        <blockquote>
                            <p>Bấm vào là hiện hết trơn á</p><cite>spidermoon</cite>
                        </blockquote>
                    </div>
                </div>

                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1">
                    <div class="sl-slide-inner">
                        <div class="bg-img bg-img-5"></div>
                        <h2>Tất cả sản phẩm</h2>
                        <blockquote>
                            <p>Bấm vào là hiện hết trơn á</p><cite>spidermoon</cite>
                        </blockquote>
                    </div>
                </div>
            </div><!-- /sl-slider -->

            <nav id="nav-dots" class="nav-dots">
                <span class="nav-dot-current"></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </nav>

        </div><!-- /slider-wrapper -->



    </div>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.ba-cond.min.js"></script>
    <script type="text/javascript" src="js/jquery.slitslider.js"></script>
    <script type="text/javascript">
        $(function() {

            var Page = (function() {

                var $nav = $('#nav-dots > span'),
                    slitslider = $('#slider').slitslider({
                        onBeforeChange: function(slide, pos) {

                            $nav.removeClass('nav-dot-current');
                            $nav.eq(pos).addClass('nav-dot-current');

                        }
                    }),

                    init = function() {

                        initEvents();

                    },
                    initEvents = function() {

                        $nav.each(function(i) {

                            $(this).on('click', function(event) {

                                var $dot = $(this);

                                if (!slitslider.isActive()) {

                                    $nav.removeClass('nav-dot-current');
                                    $dot.addClass('nav-dot-current');

                                }

                                slitslider.jump(i + 1);
                                return false;

                            });

                        });

                    };

                return {
                    init: init
                };

            })();

            Page.init();

            /**
             * Notes: 
             * 
             * example how to add items:
             */

            /*
            
            var $items  = $('<div class="sl-slide sl-slide-color-2" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1"><div class="sl-slide-inner bg-1"><div class="sl-deco" data-icon="t"></div><h2>some text</h2><blockquote><p>bla bla</p><cite>Margi Clarke</cite></blockquote></div></div>');
            
            // call the plugin's add method
            ss.add($items);

            */

        });
    </script>

    <?php
    include("php/connectdb.php"); //ket noi csdl   
    ?>
    <!--body -->
    <div class="container-s">
        <div id="wrapper-all">
            <!-- display danh sach san pham - loại 1 -->
            <div class="sliderows">
                <div class="item-fix">
                    <div class="nav-catalog">
                        <h2 class="name-parent-items">
                            <a href="#">CLOTHES - PANTS</a>
                        </h2>
                        <!-- menu chinh - luu loai sp, submenu - nhung san pham thuoc loai nao -->
                        <!-- lay du lieu tu ban sub_menu  -->
                        <div class="sub-menu">
                            <?php
                            $clo_res = mysqli_query($conn, "SELECT * FROM submenu WHERE catalog_id= 25") or die('Có lỗi xin kiểm tra lại!');
                            $clo_items = mysqli_fetch_array($clo_res); //tra ve kq cho clo_res
                            echo "<a href='php/products.php?id_menu=" . $clo_items['catalog_id'] . "'>Xem tất cả sản phẩm</a>";
                            ?>
                        </div>
                        <!--end sub-menu-->
                        <?php
                        $clothes_res = mysqli_query($conn, "SELECT * FROM submenu WHERE catalog_id= 25") or die('Có lỗi xin kiểm tra lại!');
                        while ($clothes_items = mysqli_fetch_array($clothes_res)) {
                        ?>
                            <h2 class="sub-menu">
                            <?php
                            echo "<a href='php/products.php?id_menu=" . $clothes_items['submenu_id'] . "'>" . $clothes_items['sub_name'] . " </a>";
                            echo "</h2>";
                        }
                            ?>
                    </div>
                </div>
                <!-- end nav-catalog -->

                <!-- hien thi san pham len trang chu -->
                <div class="product-row">
                    <div class="product-row-item1">
                        <?php
                        $clothes_query = "SELECT * FROM sanpham where catalog_id = 25 limit 8"; //so luong sp hien thi tren mot product-row
                        $clothes_res = mysqli_query($conn, $clothes_query) or die('Có lỗi xin kiểm tra lại!');
                        while ($clothes_items = mysqli_fetch_array($clothes_res)) {
                        ?>
                            <div class="product-item">
                                <div class="product-row-a">
                                    <?php
                                    // di den trang hien thi thong tin chi tiet cua san pham
                                    echo "
                                        <a href='php/productdetails.php?id=" . $clothes_items['product_id'] . "' class='images'>
                                            <img alt='" . $clothes_items['product_name'] . "' src='images/" . $clothes_items['product_image'] . "'>
                                        </a>

                                        <h2>
                                            <a title='" . $clothes_items['product_name'] . "' href='php/productdetails.php?id=" . $clothes_items['product_id'] . "' style='text-align: center;display: inherit;font: 16px/20px \"Roboto\",Helvetica,Arial,sans-senif;'>" . $clothes_items['product_name'] . "</a>
                                        </h2>
                                        
                                        <!-- hien thi gia san pham-->
                                        <div class='price' style='text-align: center;'>" . number_format($clothes_items['price']) . " VNĐ</div>

                                        <div class='ratings'>
                                            <div class='rating-box'></div>
                                        </div>
                                        <!-- them sp truc tiep vao gio hang -->
                                        <a href='php/addtocart.php?id=" . $clothes_items['product_id'] . "'><div class='add-cart''><i class='fas fa-shopping-cart'></i> Add to cart </div></a>
                                        ";
                                    ?>
                                </div>
                                <!-- end product-item-a -->
                            </div>
                            <!-- end product-item -->
                        <?php
                        }
                        ?>
                    </div>
                </div>

            </div>

            <!-- display danh sach san pham - loại 2 -->
            <div class="sliderows">
                <div class="nav-catalog">
                    <h2 class="name-parent-items">
                        <a href="#">BACKPACK</a>
                    </h2>
                    <!-- menu chinh - luu loai sp, submenu - nhung san pham thuoc loai nao -->
                    <!-- lay du lieu tu ban sub_menu  -->
                    <div class="sub-menu">
                        <?php
                        $b_pack_res = mysqli_query($conn, "SELECT * FROM submenu WHERE catalog_id= 26") or die('Có lỗi xin kiểm tra lại!');
                        $b_pack_items = mysqli_fetch_array($b_pack_res); //tra ve kq cho clo_res
                        echo "<a href='php/products.php?id_menu=" . $b_pack_items['catalog_id'] . "'>Xem tất cả sản phẩm</a>";
                        ?>
                    </div>
                    <!--end sub-menu-->
                    <?php
                    $backpack_res = mysqli_query($conn, "SELECT * FROM submenu WHERE catalog_id= 26") or die('Có lỗi xin kiểm tra lại!');
                    while ($backpack_items = mysqli_fetch_array($backpack_res)) {
                    ?>
                        <h2 class="sub-menu">
                        <?php
                        echo "<a href='php/products.php?id_menu=" . $backpack_items['submenu_id'] . "'>" . $backpack_items['sub_name'] . " </a>";
                        echo "</h2>";
                    }
                        ?>
                </div>
                <!-- end nav-catalog -->

                <!-- hien thi san pham len trang chu -->
                <div class="product-row">
                    <div class="product-row-item1">
                        <?php
                        $backpack_query = "SELECT * FROM sanpham where catalog_id = 26 limit 8"; //so luong sp hien thi tren mot product-row
                        $backpack_res = mysqli_query($conn, $backpack_query) or die('Có lỗi xin kiểm tra lại!');
                        while ($backpack_items = mysqli_fetch_array($backpack_res)) {
                        ?>
                            <div class="product-item">
                                <div class="product-row-a">
                                    <?php
                                    // di den trang hien thi thong tin chi tiet cua san pham
                                    echo "
                                        <a href='php/productdetails.php?id=" . $backpack_items['product_id'] . "' class='images'>
                                            <img alt='" . $backpack_items['product_name'] . "' src='images/" . $backpack_items['product_image'] . "'>
                                        </a>

                                        <h2>
                                            <a title='" . $backpack_items['product_name'] . "' href='php/productdetails.php?id=" . $backpack_items['product_id'] . "' style='text-align: center;display: inherit;font: 16px/20px \"Roboto\",Helvetica,Arial,sans-senif;'>" . $backpack_items['product_name'] . "</a>
                                        </h2>
                                        
                                        <!-- hien thi gia san pham-->
                                        <div class='price' style='text-align: center;'>" . number_format($backpack_items['price']) . " VNĐ</div>

                                        <div class='ratings'>
                                            <div class='rating-box'></div>
                                        </div>
                                        <!-- them sp truc tiep vao gio hang -->
                                        <a href='php/addtocart.php?id=" . $backpack_items['product_id'] . "'><div class='add-cart''><i class='fas fa-shopping-cart'></i> Add to cart </div></a>
                                        ";
                                    ?>
                                </div>
                                <!-- end product-item-a -->
                            </div>
                            <!-- end product-item -->
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <?php include_once "footer.php";
    ?>
    <script src="js/js_search.js"></script>
    <script src="js/loginindex.js"></script>
    <!-- <script src="js/search.js"></script> -->
</body>

</html>