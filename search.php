<?php
    session_start();
    include_once "php/connectdb.php";
    include("php/functions.php");
    $title = $_POST['searchBar'];
    $titlenew = trim($title);
    $arr_titlenew = explode(' ',$titlenew);
    $titlenew = implode('%', $arr_titlenew);
    $titlenew = '%'.$titlenew.'%';
    $item_per_page = 10;
    $current_page =  1;
    $offset =($current_page - 1)*$item_per_page;
    
    $sql = mysqli_query($conn,"SELECT * FROM sanpham WHERE 	product_name LIKE '$titlenew' ORDER BY product_id DESC LIMIT ".$item_per_page." OFFSET ".$offset."");
   
    $totalRecords = mysqli_query($conn,"SELECT * FROM sanpham WHERE product_name LIKE '$titlenew'");
    //var_dump($totalRecords);exit;
    $totalRecords = $totalRecords->num_rows;
    $totalpages = ceil($totalRecords/$item_per_page);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tìm Kiếm</title>
    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css"
        integrity="sha512-rqQltXRuHxtPWhktpAZxLHUVJ3Eombn3hvk9PHjV/N5DMUYnzKPC1i3ub0mEXgFzsaZNeJcoE0YHq0j/GFsdGg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/custom.css" />
    <script type="text/javascript" src="js/modernizr.custom.79639.js"></script>
</head>
</head>

<body>
    <?php 
error_reporting(E_ALL ^ E_WARNING); 
 if($_SESSION['user_id'])
 {
    include("headerlogin.php");
}
 else{
     
    include("header.php");
}
 ?>
    <div class=" demo-2">
        <div id="slider" class="sl-slider-wrapper">
            <div class="sl-slider">
                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25"
                    data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                    <div class="sl-slide-inner">
                        <div class="bg-img bg-img-1 bg-find"></div>
                        <h2>Tìm kiếm kết quả </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--body -->
    <div class="container-s">
        <div id="wrapper-all">
            <!-- display danh sach san pham - loại 1 -->>
            <h2 class="name-parent-items">
                <label> Tìm kiếm kết quả cho <?php echo $title;?> </label>
            </h2>
            <?php if(mysqli_num_rows($sql) == 0)
            {
            ?>
            <h2 class="name-parent-items">
                <br>
                <label style="font-size: 18px;color: #B8860B;padding: 23px;padding-left: 170px;"> Không tìm thấy mặt hàng</label>
            </h2>
            <?php } ?>
        </div>


        <?php if(mysqli_num_rows($sql) > 0)
        {
            while($row = mysqli_fetch_array($sql))
            {  
        ?>
        <div id="wrapper-all">
            <div class="product-row">
                <div class="product-row-item1">
                    <div class="product-item"style=" padding:20px; display: flex;justify-content: center;">
                        <div class="product-row-a" style="">
                            <?php
                                    // di den trang hien thi thong tin chi tiet cua san pham
                                    echo "
                                        <a style='text-align: center;' href='php/productdetails.php?id=" . $row['product_id'] . "' class='images'>
                                            <img alt='" . $row['product_name'] . "' src='images/" . $row['product_image'] . "'>
                                        </a>

                                        <h2>
                                            <a title='" . $row['product_name'] . "' href='php/productdetails.php?id=" . $row['product_id'] . "' 
                                            style='text-align: center;display: inherit;font: 16px/20px \"Roboto\",Helvetica,Arial,sans-senif;'>" .  $row['product_name'] . "</a>
                                        </h2>
                                        
                                        <!-- hien thi gia san pham-->
                                        <div class='price' style='text-align: center;'>" . number_format($row['price']) . " VNĐ</div>

                                        <div class='ratings'>
                                            <div class='rating-box'></div>
                                        </div>
                                        <!-- them sp truc tiep vao gio hang -->
                                        <a href='php/addtocart.php?id=" . $row['product_id'] . "'><div class='add-cart''><i class='fas fa-shopping-cart'></i> Add to cart </div></a>
                                        ";
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }} ?>
    </div>
    <?php include_once "Page-paging";?>
    <!-- footer -->
    <?php include_once "footer.php";
    ?>
    <script src="js/js_search.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/loginindex.js"></script>
</body>

</html>