<!DOCTYPE html>
<html lang="en">
<?php
//include_once "header.php";
include("php/connectdb.php"); //ket noi csdl   
$total_product = 0;
if (!empty($_SESSION['cart'])) $total_product = count($_SESSION['cart']);
?>

<head>

    <title>Cart</title>
    <link rel="stylesheet" href="css/style.css" />

</head>

<body>
    <?php
    error_reporting(E_ALL ^ E_WARNING); //tránh cảnh cáo
    if ($_SESSION['user_id']) {
        include("headerlogin.php");
    } else {

        include("header.php");
    }
    ?>
    <div class="navigations"></div>
    <form method="post">
        <div class="main-shopping">
            <p class="cart-infor">
                <?php if ($_SESSION['cart'] != NULL) {
                    echo "Thông tin giỏ hàng";
                } else {
                    echo "Giỏ hàng chưa có sản phẩm";
                } ?>
            </p>
            <div class="order-cart-1">
                <ul class="table-cart">
                    <li class="sanpham_cart">SẢN PHẨM</li>
                    <li class="dongia_cart">ĐƠN GIÁ</li>
                    <li class="soluong_cart">SỐ LƯỢNG</li>
                    <li class="thanhtien_cart">THÀNH TIỀN</li>
                </ul>

                <?php
                // tổng tất cả các loại sản phẩm trong giỏ
                $sum_all_product = 0;
                // tổng số lượng 1 loại sản phẩm
                $sum_product = 0;
                if ($_SESSION['cart'] != NULL) {
                    foreach ($_SESSION['cart'] as $id => $total_product) {
                        // lưu số lương sản phẩm vào mảng
                        $arr_id[] = $id;
                    }
                    // chuyển mảng thành chuỗi
                    $string_id = implode(',', $arr_id);
                    // truy vấn đến bảng sản phẩm, sắp xếp sp theo thứ tự tăng dầng theo id_sp
                    $item_query = "SELECT * FROM  sanpham WHERE product_id IN ($string_id) ORDER BY product_id ASC";
                    $item_result = mysqli_query($conn, $item_query) or die('Có lỗi xin kiểm tra lại!');
                    while ($rows = mysqli_fetch_array($item_result)) {
                ?>
                        <!-- phần hiển thị giỏ hàng -->
                        <ul class="bottom_cart">
                            <li class="sanpham_cart">
                                <img src="images/<?php echo $rows['product_image']; ?>" class="cart_image">
                                <b class="cart-title"><?php echo $rows['product_name']; ?></b>
                                <div class="delete-cart" style="text-align: center;font-size: 15px;"><a href="<?php echo $base ?>/php/delete-product.php?id=<?php echo $rows['product_id']; ?>" class="delete-product"><i class="fa fa-shopping-cart"></i>Xóa</a></div>
                            </li>
                            <li class="dongia_cart"><?php echo number_format($rows['price']); ?> VNĐ</li>
                            <li class="soluong_cart"><input type="text" name="num[<?php echo $rows['product_id']; ?>]" value="<?php echo $_SESSION['cart'][$rows['product_id']]; ?>" size="3" class="update_cart_text" /></li>
                            <li class="thanhtien_cart"><?php echo number_format($rows['price'] * $_SESSION['cart'][$rows['product_id']]); ?>
                                VNĐ
                            </li>
                        </ul>
                <?php
                        $sum_product += $_SESSION['cart'][$rows['product_id']];
                        $sum_all_product += $rows['price'] * $_SESSION['cart'][$rows['product_id']];
                    }
                    echo '</div>

                        <div class="bottom_button">
                        <p class="update_cart">
                            <input type="submit" name="update_cart" value="CẬP NHẬT" class="update_again_cart"/>
                                <a href="' . $base . '/php/pay.php" class="dat_hang" style="display:block;">THANH TOÁN</a>  
                        </p>

                        <p class="sum_money">Tổng sản phẩm:<strong class="sum_sp"> ' . $sum_product . '</strong><br/>Tất cả: <strong class="sum_sp"> ' . number_format($sum_all_product) . ' VNĐ</strong></p>
                        <a href="https://localhost/web/" class="back-page"> Tiếp tục mua sắm</a>
                        <a href="/php/delete-product.php?id=0" class="delete_all">Xóa giỏ hàng</a>
                        </div>
                        <div class="clear-px"></div> ';
                }
                ?>
    </form>
    <?php
    //include_once "footer.php";
    ?>
</body>

</html>