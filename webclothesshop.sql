-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 09, 2021 lúc 07:10 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `webclothesshop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `level` int(4) NOT NULL,
  `idgroup` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `fullname`, `level`, `idgroup`) VALUES
(1, 'admin', '25d55ad283aa400af464c76d713c07ad', 'admin@gmail.com', 'admin', 1, 1),
(2, 'tester', '25f9e794323b453885f5181f1b624d0b', 'tester@gmail.com', 'tester', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(12) NOT NULL,
  `banner_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `banner_image` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_name`, `banner_image`) VALUES
(1, 'Flashsale', 'slideshow1.jpg'),
(2, 'Flashsale', 'slideshow2.jpg'),
(3, 'Flashsale', 'slideshow3.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `donhang`
--

CREATE TABLE `donhang` (
  `transaction_id` int(15) NOT NULL,
  `id` int(15) NOT NULL,
  `product_id` int(15) NOT NULL,
  `quantity` int(100) NOT NULL DEFAULT 0,
  `total` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `status` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `giaodich`
--

CREATE TABLE `giaodich` (
  `id` int(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `user_id` int(15) NOT NULL DEFAULT 0,
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `payment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_info` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notice` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `catalog_id` int(12) NOT NULL,
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `menu_valid` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`catalog_id`, `menu_name`, `menu_valid`) VALUES
(25, 'CLOTHES', 'quan_ao'),
(26, 'BACKPACKS', 'balo'),
(27, 'ACCESSORIES', 'vi-bop');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `product_id` int(12) NOT NULL,
  `catalog_id` int(12) NOT NULL,
  `sub_id` int(12) NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `discount` int(15) NOT NULL,
  `product_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_created` date NOT NULL,
  `viewer` int(12) NOT NULL,
  `source` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `size` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `color` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_menu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_sub_menu` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`product_id`, `catalog_id`, `sub_id`, `product_name`, `product_code`, `price`, `description`, `content`, `discount`, `product_image`, `date_created`, `viewer`, `source`, `size`, `color`, `parent_menu_name`, `parent_sub_menu`) VALUES
(16, 25, 3, 'Cardigan Len Degrey - CLD', '0101', '680000.0000', '', '', 0, 'product-3.jpg', '2021-10-29', 0, 'Việt Nam', 'M', 'Kem', '', ''),
(17, 25, 3, 'Gile Len Degrey - GLD', '0075', '420000.0000', '', '', 0, 'product-9.jpg', '2021-10-29', 0, 'Việt Nam', 'M', 'Kem', '', ''),
(18, 25, 3, 'Áo Thun Vằn Vèo Trắng - ATVV Trắng', '0176', '320000.0000', '', '', 0, 'product-5.jpg', '2021-10-29', 0, 'Việt Nam', 'M, L', 'Trắng', '', ''),
(19, 25, 3, 'Áo Thun Chữ Vằn Vèo - ATVV Đen', '0075', '320000.0000', '', '', 0, 'product-6.jpg', '2021-10-29', 0, 'Việt Nam', 'M, L', 'Đen', '', ''),
(20, 25, 3, 'Áo Khoác Nấm Xanh - AKND Xanh', '0185', '390000.0000', '', '', 0, 'product-4.jpg', '2021-10-29', 0, 'Việt Nam', 'M, L', 'Xanh Dương', '', ''),
(21, 25, 3, 'Hút Đi Logo Nhiều Màu - HLNM', '0081', '380000.0000', '', '', 0, 'product-8.jpg', '2021-10-29', 0, 'Việt Nam', 'L', 'Đen', '', ''),
(24, 25, 3, 'Áo Giờ Giải Lao Đen - GGL Đen', '0145', '380000.0000', '', '', 0, 'product-7.jpg', '2021-10-29', 0, 'Việt Nam', 'M, L', 'Đen', '', ''),
(27, 25, 3, 'Ice Cream Tee Blue - ICT Blue', '0154', '290000.0000', '', '', 0, 'product-1.jpg', '2021-10-29', 0, 'Việt Nam', 'M, L', 'Color full', '', ''),
(28, 25, 3, 'Sơ Mi UFO Degrey - SMU', '0155', '290000.0000', '', '', 0, 'product-2.jpg', '2021-10-29', 0, 'Việt Nam', 'M, L', 'Đen', '', ''),
(31, 26, 1, 'Degrey Galaxy Backpack - DGB', '0102', '390000.0000', '', '', 0, 'ba-lo-1.jpg', '2021-10-29', 0, 'Việt Nam', '40 x 30 x 13 cm', 'Hồng', '', ''),
(32, 26, 1, 'Basic Backpack Degrey Holo - BBDH', '0075', '390000.0000', '', '', 0, 'ba-lo-2.jpg', '2021-10-29', 0, 'Việt Nam', '40 x 30 x 13 cm', 'Đen', '', ''),
(33, 26, 1, 'Balo Họa Tiết Chữ Degrey - DLBP', '0176', '420000.0000', '', '', 0, 'ba-lo-3.jpg', '2021-10-29', 0, 'Việt Nam', '40 x 30 x 13 cm', 'Trắng', '', ''),
(34, 26, 1, 'Balo Degrey Caro Nâu - BDC Nâu', '0077', '420000.0000', '', '', 0, 'ba-lo-4.jpg', '2021-10-29', 0, 'Việt Nam', '40 x 30 x 13 cm', 'Nâu', '', ''),
(35, 26, 1, 'Balo Degrey Thêu Nhiều Màu - BDT', '0103', '420000.0000', '', '', 0, 'ba-lo-5.jpg', '2021-10-29', 0, 'Việt Nam', '40 x 30 x 13 cm', 'Đen', '', ''),
(36, 26, 1, 'Balo Hạt Nhân Degrey - BHND', '0079', '420000.0000', '', '', 0, 'ba-lo-6.jpg', '2021-10-29', 0, 'Việt Nam', '40 x 30 x 13 cm', 'Đen', '', ''),
(37, 26, 1, 'Basic Backpack Degrey Xám - BBD Xám', '0808', '390000.0000', '', '', 0, 'ba-lo-7.jpg', '2021-10-29', 0, 'Việt Nam', '40 x 30 x 13 cm', 'Trắng', '', ''),
(38, 26, 1, 'TieDye Backpack Dark Green - TDBP', '0013', '420000.0000', '', '', 0, 'ba-lo-8.jpg', '2021-10-29', 0, 'Việt Nam', '40 x 30 x 13 cm', 'Đen', '', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `submenu`
--

CREATE TABLE `submenu` (
  `submenu_id` int(15) NOT NULL,
  `sub_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `valid_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `catalog_id` int(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `submenu`
--

INSERT INTO `submenu` (`submenu_id`, `sub_name`, `valid_name`, `catalog_id`) VALUES
(1, 'BACKPACKS', 'balo', 26),
(2, 'PANTS', 'quan', 25),
(3, 'CLOTHES', 'ao', 25),
(4, 'ACCESSORIES', 'phu-kien', 27),
(5, 'Ví', '#', 27);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `numberphone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `date_created` date DEFAULT current_timestamp(),
  `level` int(4) DEFAULT NULL,
  `idgroup` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `fullname`, `email`, `numberphone`, `address`, `date_created`, `level`, `idgroup`) VALUES
(1, 'admin', '123456789', 'Lâm Trung Hiếu', 'hieulam0918@gmail.com', '0123456789', 'Bạc Liêu', '2021-10-30', 1, 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `donhang`
--
ALTER TABLE `donhang`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `giaodich`
--
ALTER TABLE `giaodich`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`catalog_id`);

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`product_id`);

--
-- Chỉ mục cho bảng `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`submenu_id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `product_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
